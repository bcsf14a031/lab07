#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(){


	int fd[2];
	pipe(fd);
	int fd1[2];
	pipe(fd1);
		
	if(fork()==0){
		
		dup2(fd[0],0);
		dup2(fd1[1],1);
		close(fd[1]);
		close(fd1[0]);
		execlp("grep", "grep","ls", NULL);
	}
	if(fork()== 0){
			
			
			dup2(fd1[0],0);
			close(fd[1]);
			close(fd[0]);
			close(fd1[1]);
			execlp("wc", "wc",NULL);


		}
	dup2(fd[1],1);
	close(fd[0]);
	close(fd1[0]);
	close(fd1[1]);
	execlp("man","man", "ls", NULL );
	return 0;
}